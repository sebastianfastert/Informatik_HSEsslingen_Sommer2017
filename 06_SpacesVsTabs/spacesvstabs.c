
#include "spacesvstabs.h"

int main(int argc, char const *argv[]) {

	int tabdef = 4;
	char *filename = malloc(sizeof(char) * 100);
	char *newfileprefix = malloc(sizeof(char) * 100);
	strcpy(filename, "default.txt");
	strcpy(newfileprefix, "new");

	if (argc <= 1) {
		printf("\t-f\tfilename\n\t-t\ttablength\n\t-p\tprefix of new filename\n");
		return 0;
	}

	for (int i = 1; i < argc; i+=2) {
		if(strcmp(argv[i], "-t") == 0) {
			tabdef = *argv[i + 1] - '0';
		} else if(strcmp(argv[i], "-f") == 0) {
			strcpy(filename, argv[i + 1]);
		} else if (strcmp(argv[i], "-p") == 0) {
			strcpy(newfileprefix, argv[i + 1]);
		}
	}

	strcat(newfileprefix, filename);

	printf("definierte Tablänge:\t%d\n", tabdef);
	printf("ausgewählte Datei:\t%s\n", filename);
	printf("neue Datei:\t\t%s\n", newfileprefix);

	char *str = loadFileAsString(filename);
	if(str != NULL) {
		printf("Stringlänge:\t\t%lu\n", strlen(str));
		char *newstr = replaceAllRelevantSpaces(tabdef, str);
		if(newstr != NULL) {
			saveStringAsFile(newstr, newfileprefix);
			free(newstr);
		}
	}

	free(filename);
	free(newfileprefix);
	free(str);

	return 0;
}

char *loadFileAsString(const char* filename)
{
	FILE *file;
	int c;
	int i = 0;
	int max = BUFFERSIZE; // needed to secure that str is not too short
	char *str = calloc(BUFFERSIZE, sizeof(char)); // allocating memory for the str

	if (str == NULL) {
		printf("could not allocate memory\n");
	}

	file = fopen(filename, "r"); // loading file in read mode

	if (file == NULL) {
		printf("could not read file\n");
		return NULL;
	}

	while ((c = getc(file)) != EOF) { // iterating every character in the file
		str[i] = c; // copying all char into the string
		i++;
		if (i == max-1) { // if we're at the end of str, we make the str larger
			str = realloc(str, sizeof(char) * strlen(str) + sizeof(char) * BUFFERSIZE);
			if (str == NULL) {
				printf("could not allocate memory\n");
				return NULL;
			}
			max = sizeof(char) * strlen(str) + sizeof(char) * BUFFERSIZE;
		}
	}
	fclose(file);
	return str;
}

char *replaceAllRelevantSpaces(int tabdef, char *str)
{
	int length = strlen(str);
	printf("Stringlänge:\t\t%d\n", length);
	char *res = malloc(sizeof(char) * (1 + length));

	if (res == NULL) {
		printf("could not allocate memory\n");
		return NULL;
	}

	int j = 0;
	for (int i = 0; i < length; i++) {
		if (isTab(i, str, tabdef)) {
			res[j] = '\t';
			i += (tabdef - 1);
		} else {
			res[j] = str[i];
		}
		j++;
	}
	return res;
}

int isTab(int p, char *str, int tabdef)
{
	for (int i = 0; str[p + i] == ' '; i++) {
		if (i == (tabdef - 1)) {
			return tabdef;
		}
	}
	return 0;
}

void saveStringAsFile(char *str, char *filename)
{
	FILE *file;
	file = fopen(filename, "w");
	fprintf(file, "%s", str);
	fclose(file);
}
