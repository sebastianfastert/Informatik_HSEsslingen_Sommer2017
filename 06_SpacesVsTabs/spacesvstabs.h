#ifndef SPACESVSTABS_H
#define SPACESVSTABS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFERSIZE 1024
#define TRUE 1
#define FALSE 0

char *loadFileAsString(const char* filename);
char *replaceAllRelevantSpaces(int tabdef, char *str);
void saveStringAsFile(char *str, char *filename);
int isTab(int p,char *str, int tabdef);

#endif // SPACESVSTABS_H
