
#ifndef FASTERT_MYENTRY_H
#define FASTERT_MYENTRY_H

#include <stddef.h>
#include <stdlib.h>

struct myEntry {
    int val;
    struct myEntry* successor;
};

typedef struct myEntry MYENTRY;

MYENTRY* getSuccessor(MYENTRY* current);

#endif // FASTERT_MYENTRY_H
