
#include <stdio.h>
#include "mystack.h"

int main() {
    MYSTACK* mystack = createStack();
    push(3, mystack);
    printf("%d\n", mystack->count);
    push(2, mystack);
    printf("%d\n", mystack->count);
    push(1, mystack);
    printf("%d\n", mystack->count);

    printf("pop: %d\n", pop(mystack)->val);
    printf("%d\n", mystack->count);
    printf("pop: %d\n", pop(mystack)->val);
    printf("%d\n", mystack->count);
    printf("pop: %d\n", pop(mystack)->val);
    printf("%d\n", mystack->count);
    pop(mystack);
    MYENTRY* ent = pop(mystack);
    if (ent != (MYENTRY*)NULL) {
        printf("pop: %d\n", ent->val);
    } else {
        printf("err\n");
    }
    return 0;
}
