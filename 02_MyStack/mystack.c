
#include "mystack.h"

MYSTACK* createStack()
{
    MYSTACK* temp = malloc(sizeof(MYSTACK));
    temp->count = 0;
    temp->top = NULL;
    return temp;
}

void push(int val, MYSTACK* mystack)
{
    MYENTRY* newTop = malloc(sizeof(MYENTRY));
    newTop->val = val;
    newTop->successor = mystack->top;
    mystack->top = newTop;
    mystack->count++;
}

MYENTRY* pop(MYSTACK* mystack)
{
    if(mystack->count != 0) {
        MYENTRY* temp = malloc(sizeof(MYENTRY));
        temp = mystack->top;
        mystack->top = mystack->top->successor;
        mystack->count--;
        return temp;
    } else {
        printf("Entry does not exist.\n");
        return (MYENTRY*)NULL;
    }
}
