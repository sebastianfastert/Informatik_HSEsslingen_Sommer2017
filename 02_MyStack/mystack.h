
#ifndef FASTERT_MYSTACK_H
#define FASTERT_MYSTACK_H

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "myentry.h"

struct myStack {
    int count;
    MYENTRY* top;
};

typedef struct myStack MYSTACK;

MYSTACK* createStack();
void push(int val, MYSTACK* mystack);
MYENTRY* pop(MYSTACK* mystack);

#endif // FASTERT_MYSTACK_H
