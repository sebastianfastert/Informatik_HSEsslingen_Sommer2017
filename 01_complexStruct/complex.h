#ifndef SEBASTIANFASTERT_COMPLEX_H
#define SEBASTIANFASTERT_COMPLEX_H

#include "stdio.h"

// struct that represents a complex number
typedef struct {
    double re;
    double im;
} Complex;

// function to add two complex numbers
// complex numbers need to be based on the struct complex
// returns a complex number based on the struct complex
Complex sumComplex(const Complex* com1, const Complex* com2);

// function to subtract two complex numbers
// complex numbers need to be based on the struct complex
// returns a complex number based on the struct complex
Complex subComplex(const Complex* com1, const Complex* com2);

// function to multiply two complex numbers
// complex numbers need to be based on the struct complex
// returns a complex number based on the struct complex
Complex mulComplex(const Complex* com1, const Complex* com2);

// function to conjugate a complex number
// argument takes the address of the struct complex
void conjugate(Complex *complex);

// prints a complex number to the console
// argument takes the address of the struct complex
void printComplex(Complex complex);

#endif // SEBASTIANFASTERT_COMPLEX_H
