#include "complex.h"

Complex sumComplex(const Complex* com1, const Complex* com2)
{
    Complex result = {
        result.re = com1->re + com2->re,
        result.im = com1->im + com2->im
    };
    return result;
}

Complex subComplex(const Complex *com1, const Complex *com2)
{
    Complex result = {
        com1->re - com2->re,
        com1->im - com2->im
    };
    return result;
}

Complex mulComplex(const Complex* com1, const Complex* com2)
{
    Complex result = {
        com1->re * com2->re - com1->im * com2->im,
        com1->re * com2->im + com1->im * com2->re
    };
    return result;
}

void conjugate(Complex *complex)
{
    complex->im *= -1;
}

void printComplex(Complex complex)
{
    printf("%.2f %+.2f * i\n", complex.re, complex.im);
}


// int main(int argc, char const *argv[]) {
//     Complex com1 = {
//         1,
//         2
//     };
//     Complex com2 = {
//         2,
//         4
//     };
//     printComplex(sumComplex(&com1, &com2));
//     printComplex(subComplex(&com1, &com2));
//     printComplex(mulComplex(&com1, &com2));
//     conjugate(&com1);
//     printComplex(com1);
//     return 0;
// }
