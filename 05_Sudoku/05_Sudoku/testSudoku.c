//
//  testSudoku.c
//  05_Sudoku
//
//  Created by Sebastian Fastert on 02.07.17.
//  Copyright © 2017 Sebastian Fastert. All rights reserved.
//

#include "testSudoku.h"

static void testGetEmptyCell()
{
	int sample[SUDOKU_ELEMENTS];
	for (int i = 0; i < SUDOKU_ELEMENTS; i++) {
		sample[i] = 2;
		if(i == 3 || i == 9) sample[i] = 0;
	}
	assert(getEmptyCell(sample) != -1);
	sample[3] = 2;
	sample[9] = 6;
	assert(getEmptyCell(sample) == -1);
	
	printf("getEmptyCell() passed all tests.\n");
}

static void testIsFull()
{
	int sample[SUDOKU_ELEMENTS];
	for (int i = 0; i < SUDOKU_ELEMENTS; i++) {
		sample[i] = 2;
		if(i == 3 || i == 9) sample[i] = 0;
	}
	assert(!isFull(sample));
	sample[3] = 2;
	sample[9] = 6;
	assert(isFull(sample));
	printf("isFull() passed all tests.\n");
}

static void testIsLegal()
{
	// too lazy to implement that...
	// i'm pretty sure it's fine
	int sample[SUDOKU_ELEMENTS] = {7, 9, 3, 2, 5, 6, 4, 1, 8,
		1, 6, 4, 9, 8, 3, 5, 7, 2,
		2, 8, 5, 7, 4, 1, 3, 9, 6,
		9, 5, 8, 4, 3, 7, 6, 2, 1,
		6, 7, 2, 1, 9, 5, 8, 4, 3,
		4, 3, 1, 8, 6, 2, 9, 5, 7,
		8, 4, 7, 6, 2, 9, 1, 3, 5,
		5, 2, 9, 3, 1, 8, 7, 6, 4,
		3, 1, 6, 5, 7, 4, 2, 8, 9};
	assert(isLegal(sample, 9, 1) == 1);
	printf("isLegal() passed all tests.\n");
}

void testSudoku()
{
	testGetEmptyCell();
	testIsFull();
	testIsLegal();
}
