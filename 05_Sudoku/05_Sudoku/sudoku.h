//
//  sudoku.h
//  05_Sudoku
//
//  Created by Sebastian Fastert on 01.07.17.
//  Copyright © 2017 Sebastian Fastert. All rights reserved.
//

#ifndef sudoku_h
#define sudoku_h

#include <stdio.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define SUDOKU_ELEMENTS 81
#define MIN_VAL 1
#define MAX_VAL 9
#define EMPTY_VAL 0

void getSudokuInput(int *grid);
int getEmptyCell(int *grid);
int isFull(int *grid);
int isLegal(int *grid, int currentCell, int currentVal);
int hasSolution(int *grid);
void printSod(int *grid);


#endif /* sudoku_h */
