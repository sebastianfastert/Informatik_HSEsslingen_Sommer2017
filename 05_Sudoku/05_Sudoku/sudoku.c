//
//  sudoku.c
//  05_Sudoku
//
//  Created by Sebastian Fastert on 01.07.17.
//  Copyright © 2017 Sebastian Fastert. All rights reserved.
//

#include "sudoku.h"

void getSudokuInput(int *grid)
{
	char in[81];
	fflush(stdin);
	gets(in);
	for (int j = 0; j < 81; j++) {
		grid[j] = in[j] - '0';
	}
	for (int j = 0; j < 81; j++) {
		printf("%d", grid[j]);
	}
	printf("\n");
}


int getEmptyCell(int *grid)
{
	for (int i = 0; i < SUDOKU_ELEMENTS; i++) {
		if (grid[i] == EMPTY_VAL) {
			return i;
		}
	}
	return -1;
}

int isFull(int *grid)
{
	for (int i = 0; i < SUDOKU_ELEMENTS; i++) {
		if (grid[i] == EMPTY_VAL) {
			return FALSE;
		}
	}
	return TRUE;
}

int isLegal(int *grid, int currentCell, int currentVal)
{
	// check column
	for (int i = currentCell % 9; i < SUDOKU_ELEMENTS; i+=9) {
		if (i != currentCell && currentVal == grid[i]) {
			return FALSE;
		}
	}
	// check row
	for (int i = (currentCell / 9) * 9; i < (currentCell / 9) * 9 + 9; i++) {
		if (i != currentCell && currentVal == grid[i]) {
			return FALSE;
		}
	}
	// check box
	int startCol = ((currentCell % 9) / 3) * 3;
	int startCell = (currentCell / 27) * 27 + startCol;
	for (int i = startCell; i < startCell + 3; i++){
		if (i != currentCell && currentVal == grid[i]) {
			return FALSE;
		}
		if(i+9 != currentCell && currentVal == grid[i+9])
		{
			return FALSE;
		}
		if(i+18 != currentCell && currentVal == grid[i+18])
		{
			return FALSE;
		}
	}
	return TRUE;
}

int hasSolution(int grid[])
{
    int currentVal = MIN_VAL;
    int currentCell = 0;
	int solved;

    if (isFull(grid)) {
        solved = TRUE;
    } else {
		solved = FALSE;
		currentCell = getEmptyCell(grid);
		while (!solved && (currentVal <= MAX_VAL)){
			if (isLegal(grid, currentCell, currentVal)) {
				grid[currentCell] = currentVal;
				if (hasSolution(grid)) {
					solved = TRUE;
				} else {
					grid[currentCell] = EMPTY_VAL;
				}
			}
			currentVal++;
		}
	}
    return solved;
}

void printSod(int *grid)
{
	for (int i = 0; i < SUDOKU_ELEMENTS; i++) {
		if (i % 3 == 0) {
			printf(" ");
		}
		if (i % 27 == 0) {
			printf("\n ");
		}
		printf(" %d ", grid[i]);
		if (i % 9 == 8) {
			printf("\n");
		}
	}
	printf("\n");
}
