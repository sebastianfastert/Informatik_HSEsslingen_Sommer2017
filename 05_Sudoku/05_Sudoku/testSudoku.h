//
//  testSudoku.h
//  05_Sudoku
//
//  Created by Sebastian Fastert on 02.07.17.
//  Copyright © 2017 Sebastian Fastert. All rights reserved.
//

#ifndef testSudoku_h
#define testSudoku_h

#include <stdio.h>
#include <assert.h>
#include "sudoku.h"

void testSudoku();

#endif /* testSudoku_h */
