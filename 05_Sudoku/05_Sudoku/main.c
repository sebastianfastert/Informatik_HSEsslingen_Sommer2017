//
//  main.c
//  05_Sudoku
//
//  Created by Sebastian Fastert on 01.07.17.
//  Copyright © 2017 Sebastian Fastert. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include "sudoku.h"
#include "testSudoku.h"

int main(int argc, const char * argv[]) {
    
    clock_t a;
    clock_t b;
    int grid[81] = {0,0,3,0,0,6,4,0,8,0,6,4,9,0,0,0,7,0,2,8,0,0,0,0,0,9,6,0,5,8,4,0,7,0,2,0,0,0,0,1,0,5,0,0,0,0,3,0,8,0,0,9,0,0,8,4,0,0,0,0,0,0,5,0,2,0,0,0,8,7,6,0,3,1,6,5,7,0,2,0,0};
    
    char ein;
    fflush(stdin);
    //ein = getchar();
    double g;
    printf("%3.1lf", 3.0);
    scanf("%lf", &g);
    printf("%3.1lf", g);
    /*if(ein != 'n')
    {
        getSudokuInput(grid);
    }*/
    
    testSudoku();
    
    printf("\nUnsolved Sudoku:\n");
    printSod(grid);
    
    a = clock();
    hasSolution(grid);
    b = clock();
    
    printf("Time to solve the Sudoku: %ld ms\n", b-a);
    
    printf("\nSolved Sudoku:\n");
    printSod(grid);
    
    return 0;
}
