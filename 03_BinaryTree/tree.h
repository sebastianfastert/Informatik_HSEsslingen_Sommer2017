
#ifndef FASTERT_TREE_H
#define FASTERT_TREE_H

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "node.h"

// to use tree: define a root node and call the insert function with it

// insert a node in a tree
void insert(int value, NODE** current);

// prints all values under current node in in-order
void print_in_order(NODE* current);

// prints all values under current node in pre-order
void print_pre_order(NODE* current);

// returns 1 if the value is in the tree
// returns 0 if the value is not in the tree
int contains(int val, NODE* current);

// returns the number of nodes under the current node
int getTreeSize(NODE* current, int currentCount);

// returns node that has the value int val
// if it returns NULL, there happened an error along the way
NODE* getNode(int val, NODE* current);

void deleteNode(NODE* node);


NODE* getLastLeft(NODE* current);

#endif //FASTERT_TREE_H
