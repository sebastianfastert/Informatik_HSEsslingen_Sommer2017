
#include <stdio.h>
#include "node.h"
#include "tree.h"

int main(int argc, char const *argv[]) {

    NODE* root;

    insert(5, &root);
    insert(3, &root);
    insert(6, &root);
    insert(4, &root);
    insert(2, &root);
    insert(9, &root);
    insert(11, &root);
    insert(7, &root);
    insert(1, &root);
    insert(8, &root);
    insert(10, &root);
    printf("%d\n", root->value);
    printf("%d\n", root->left->value);
    printf("%d\n", root->right->value);
    printf("%d\n", root->left->right->value);
    printf("in:\n");
    print_in_order(root);
    printf("pre:\n");
    print_pre_order(root);
    for(int i = 1; i < 20; i++) {
       printf("contains %d: %d\n", i,  contains(i, root));
    }
    printf("count: %d\n", getTreeSize(root, 0));
    printf("5 hasLeft: %d\n", hasLeft(root));
    printf("5 hasRight: %d\n", hasRight(root));
    printf("1 hasLeft: %d\n", hasLeft(root->left->left->left));
    for(int i = 0; i < 20; i++) {

        if (getNode(i, root) == NULL) {
            printf("get %d: NULL\n", i);
        } else {
            printf("get %d: %d\n", i,  getNode(i, root)->value);
        }
    }
    deleteNode(getNode(9, root));
    print_in_order(root);
    return 0;
}
