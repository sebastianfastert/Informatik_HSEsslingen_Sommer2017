
#include "tree.h"

void insert(int value, NODE** current)
{
    // [1] That's the node we're going to create.
    NODE* temp = malloc(sizeof(NODE));
    temp->value = value;
    temp->left = NULL;
    temp->right = NULL;
    // [1]

    if(*current == NULL) // If we're at a position in the tree were we can insert the node
    {
        *current = temp;
    } else {
        if(value < (*current)->value)
        {
            insert(value, &(*current)->left);
        } else if(value > (*current)->value) {
            insert(value, &(*current)->right);
        } else {
            printf("Value %d already in the tree.\n", value);
        }
    }
}

void print_in_order(NODE* current)
{
    if(current != NULL) {
        print_in_order(current->left);
        printf("%d\n", current->value);
        print_in_order(current->right);
    }
}

void print_pre_order(NODE* current)
{
    if(current != NULL) {
        printf("%d\n", current->value);
        print_pre_order(current->left);
        print_pre_order(current->right);
    }
}

int contains(int val, NODE* current)
{
    int con = 0;
    if (current != NULL) {
        if(current->value == val) {
            return 1;
        } else if(current->value > val) {
            con = contains(val, current->left);
        } else if(current->value < val) {
            con = contains(val, current->right);
        }
    }
    return con;
}

int getTreeSize(NODE* current, int currentCount)
{
    if (current != NULL) {
        currentCount++;
        currentCount = getTreeSize(current->left, currentCount);
        currentCount = getTreeSize(current->right, currentCount);
    }
    return currentCount;
}

NODE* getNode(int val, NODE* current)
{
    if(current != NULL) {
        if(val < current->value) {
            return getNode(val, current->left);
        } else if (val > current->value) {
            return getNode(val, current->right);
        } else {
            return current;
        }
    }
    return NULL;
}

NODE* getLastLeft(NODE* current)
{
    NODE* temp = malloc(sizeof(NODE));
    temp = current;
    if (current->left != NULL) {
        temp = getLastLeft(current->left);
    }
    return temp;
}

void deleteNode(NODE* node)
{
    if (node->left == NULL && node->right == NULL) {
        node = NULL;
    } else if (node->left != NULL && node->right != NULL) {
        NODE* inordersuccessor = getLastLeft(node->right);
        node->value = inordersuccessor->value;
        deleteNode(inordersuccessor);
    } else if (node->left == NULL && node->right != NULL) {
        node->value = node->right->value;
        node->right = NULL;
    } else if (node->left != NULL && node->right == NULL) {
        node->value = node->left->value;
        node->left = NULL;
    }
}
