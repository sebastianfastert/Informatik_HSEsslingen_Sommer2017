
#ifndef FASTERT_TREE_H
#define FASTERT_TREE_H

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include "node.h"

// to use tree: define a root node and call the insert function with it

// insert a node in a tree
void insert(char value, char* str, NODE** current);

// prints all values under current node in in-order
void print_in_order(NODE* current);

// !! not used !!
// prints all values under current node in pre-order
void print_pre_order(NODE* current);

NODE* getNext(char val, NODE* current);

#endif //FASTERT_TREE_H
