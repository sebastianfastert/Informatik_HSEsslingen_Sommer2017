#include "morse.h"

int main(int argc, char const *argv[]) {

    NODE* morseTree = NULL;

    createMorseTree(&morseTree);

    char text[1000];

	printf("Was soll übersetzt werden?\n");
	printf("1 für Morse zu Text.\n");
	printf("2 für Text zu Morse.\n");
	printf("q um das Programm zu beenden.\n<< ");
	char menu[3];
	fflush(stdin);
	fgets(menu, 3, stdin);
	while (*menu != 'q') {
		if (*menu == '1') {
			printf("morsecode: ");
			fflush(stdin);
			fgets(text, 1000, stdin);
			printf("\n");
			printWords(text, morseTree);
			printf("\n");
        } else if(*menu == '2') {
            printf("text: ");
            fflush(stdin);
            fgets(text, 1000, stdin);
            printf("\n");
            printMorseCode(text);
            printf("\n");
        } else {
            printf("Falsche Eingabe!\n");
            printf("Was soll übersetzt werden?\n");
        }
        printf("1 für Morse zu Text.\n");
        printf("2 für Text zu Morse.\n");
        printf("q um das Programm zu beenden.\n");
        printf("<< ");
        fflush(stdin);
        fgets(menu, 3, stdin);
    }

    return 0;
}

// translates morse to text
void printWords(char* text, NODE* morseTree)
{
	printf(" >> ");
	NODE* runningNode = morseTree;
	for( ; *text; text++)								// iterating the text
	{
		if(runningNode != NULL)							// runningNode is NULL, if there is not char for the morsecode
		{
			if(*text == ' ' || text[1] == '\0'){
				printf("%c", runningNode->value);
				runningNode = morseTree;				// resets runningNode to the start of the binarytree
			}
			runningNode = getNext(*text, runningNode);	// returns the next node, dependig on the char text[i],
														// either the left or the right child of runningNode
		} else {
			printf(" ? ");								// if the morsecode is unknown. a ? is printed and
			text = nextMorseLetter(text);				// we continue with the next morseletter
			if (text == NULL) {
				return;
			}
			runningNode = morseTree;
		}
    }
    printf("\n");
}

// translates text to morse
void printMorseCode(char* text)
{
    printf(" >> ");
    for (int i = 0; i < strlen(text) - 1; i++) {
		switch (text[i]) {
			case 'A':
			case 'a':
				printf(".- ");
				break;
			case 'B':
			case 'b':
				printf("-... ");
				break;
            case 'C':
            case 'c':
                printf("-.-. ");
                break;
            case 'D':
            case 'd':
                printf("-.. ");
                break;
            case 'E':
            case 'e':
                printf(". ");
                break;
            case 'F':
            case 'f':
                printf("..-. ");
                break;
            case 'G':
            case 'g':
                printf("--. ");
                break;
            case 'H':
            case 'h':
                printf(".... ");
                break;
            case 'I':
            case 'i':
                printf(".. ");
                break;
            case 'J':
            case 'j':
                printf(".--- ");
                break;
            case 'K':
            case 'k':
                printf("-.- ");
                break;
            case 'L':
            case 'l':
                printf(".-.. ");
                break;
            case 'M':
            case 'm':
                printf("-- ");
                break;
            case 'N':
            case 'n':
                printf("-. ");
                break;
            case 'O':
            case 'o':
                printf("--- ");
                break;
            case 'P':
            case 'p':
                printf(".--. ");
                break;
            case 'Q':
            case 'q':
                printf("--.- ");
                break;
            case 'R':
            case 'r':
                printf(".-. ");
                break;
            case 'S':
            case 's':
                printf("... ");
                break;
            case 'T':
            case 't':
                printf("- ");
                break;
            case 'U':
            case 'u':
                printf("..- ");
                break;
            case 'V':
            case 'v':
                printf("...- ");
                break;
            case 'W':
            case 'w':
                printf(".-- ");
                break;
            case 'X':
            case 'x':
                printf("-..- ");
                break;
            case 'Y':
            case 'y':
                printf("-.-- ");
                break;
            case 'Z':
            case 'z':
                printf("--.. ");
                break;
            case ' ':
                printf(" ");
                break;
            case '\0':
                printf("\n");
                break;
            default:
                printf("\nLetter not supported!");
                break;
        }
    }
    printf("\n");
}

char *nextMorseLetter(char *text)
{
	while (*text != ' ') {
		if (*text == '\0') {
			return NULL;
		}
		text++;
	}
	text++;
	return text;
}

void createMorseTree(NODE** morseTree)
{
    insert(' ', " ", morseTree);
    insert('e', ".", morseTree);
    insert('t', "-", morseTree);
    insert('a', ".-", morseTree);
    insert('i', "..", morseTree);
    insert('n', "-.", morseTree);
    insert('m', "--", morseTree);
    insert('s', "...", morseTree);
    insert('u', "..-", morseTree);
    insert('r', ".-.", morseTree);
    insert('w', ".--", morseTree);
    insert('d', "-..", morseTree);
    insert('k', "-.-", morseTree);
    insert('g', "--.", morseTree);
    insert('o', "---", morseTree);
    insert('l', ".-..", morseTree);
    insert('h', "....", morseTree);
    insert('v', "...-", morseTree);
    insert('f', "..-.", morseTree);
    insert('p', ".--.", morseTree);
    insert('j', ".---", morseTree);
    insert('b', "-...", morseTree);
    insert('x', "-..-", morseTree);
    insert('c', "-.-.", morseTree);
    insert('y', "-.--", morseTree);
    insert('z', "--..", morseTree);
    insert('q', "--.-", morseTree);
}
