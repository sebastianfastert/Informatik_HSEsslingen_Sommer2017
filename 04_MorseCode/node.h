
#ifndef FASTERT_NODE_H
#define FASTERT_NODE_H

#include <stddef.h>

struct Node {
    char value;
    struct Node* left;
    struct Node* right;
};

typedef struct Node NODE;

// !! not used !!
// returns 1 if current node has a left child
// returns 0 if current node has not a left child
int hasLeft(NODE* current);

// !! not used !!
// returns 1 if current node has a right child
// returns 0 if current node has not a right child
int hasRight(NODE* current);


#endif //FASTERT_NODE_H
