
#include "node.h"

int hasLeft(NODE* current)
{
    if (current->left != 0) {
        return 1;
    } else {
        return 0;
    }
}

int hasRight(NODE* current)
{
    if (current->right != 0) {
        return 1;
    } else {
        return 0;
    }
}
