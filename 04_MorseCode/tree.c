
#include "tree.h"

// function inserts a char into a tree
// str can be seen as the "path". It's basically the morsecode for the char
void insert(char value, char* str, NODE** current)
{
    if(*current == NULL) // If we're at a position in the tree were we can insert the node
    {
		// [1] That's the node we're going to create.
		NODE* temp = malloc(sizeof(NODE));
		temp->value = value;
		temp->left = NULL;
		temp->right = NULL;
		// [1]
		*current = temp;
    } else {
        if(*str == '.') {
            str++; // string points to the next char of the morsecode
            insert(value, str, &(*current)->left);
        } else if (*str == '-') {
            str++;
            insert(value, str, &(*current)->right);
        } else {
            printf("Error while inserting elements.\n");
        }
    }
}

// function can be used to check if the chars are at the right place
void print_in_order(NODE* current)
{
    if(current != NULL) {
        print_in_order(current->left);
        printf("%c\n", current->value);
        print_in_order(current->right);
    }
}

// !! not used !!
void print_pre_order(NODE* current)
{
    if(current != NULL) {
        printf("%c\n", current->value);
        print_pre_order(current->left);
        print_pre_order(current->right);
    }
}

// !! not used !!
int getTreeSize(NODE* current, int currentCount)
{
    if (current != NULL) {
        currentCount++;
        currentCount = getTreeSize(current->left, currentCount);
        currentCount = getTreeSize(current->right, currentCount);
    }
    return currentCount;
}

// used in the printWords function
NODE* getNext(char val, NODE* current)
{
    if(val == '.') {
        return current->left;
    } else if (val == '-') {
        return current->right;
    } else {
        return current;
    }
    return NULL;
}
