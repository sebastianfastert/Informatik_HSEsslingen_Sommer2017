
#include <string.h>
#include <stdio.h>
#include "node.h" // provides struct NODE
#include "tree.h" // provides functions for the binarytree based on a root node

void createMorseTree(NODE** root);

// translates morse to text
// NODE* morseTree is the root node which is the top of the binarytree
void printWords(char* text, NODE* morseTree);

// translates text to morse
void printMorseCode(char* text);

char *nextMorseLetter(char *text);
